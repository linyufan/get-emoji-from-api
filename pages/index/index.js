// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    heo:'',
  },
  // 事件处理函数
  clickEmoji(e){
    const emojiCode = e.currentTarget.dataset.emoji    
    console.log(emojiCode)
    this.setData({
      heo:emojiCode
    })
  },
  //把从接口返回过来的表情数组进行分组，目前按24个为一组 2021.10.1 Linyufan.com
  splitArray : function (originArray, interval) {
    let newArray = []
    for (let i = 0, len = originArray.length; i < len; i += interval) {
      newArray.push(originArray.slice(i, i + interval))
    }
    return newArray
  },
  onLoad() {
    var self = this
    wx.setNavigationBarTitle({
      title: '林羽凡表情选择器',
    })
    console.log(`作者:\t林羽凡 \n 网站:\thttps://www.linyufan.com/`)
    wx.request({
      url: '实际Api地址',
      data: {
      },
      success: function (res) {
        console.log(self.splitArray(res.data, 24));
        self.setData({
          Moodarry: self.splitArray(res.data, 24),
        });
      }
    })
  },


})
